package main

import (
	"bufio"
	"fmt"
	lua "github.com/yuin/gopher-lua"
	"github.com/yuin/gopher-lua/parse"
	"io"
	luar "layeh.com/gopher-luar"
	"math"
	"os"
	"strings"
)

func main() {
	fmt.Printf("Demonstrating: Running Lua from a file\n")
	runFile() // Hello From Lua !

	fmt.Printf("Demonstrating: Calling Lua code from Go\n")
	callLua() // Hello From Lua ! Howard!

	fmt.Printf("Demonstrating: Calling Go code from Lua\n")
	callGo() //Hello from Go! Jessica!

	fmt.Printf("Demonstrating: Passing a map/slice as argument from Go to Lua\n")
	passSlice() // [2 3 5 7 11]

	fmt.Printf("Demonstrating: Returning a map/slice from Lua to Go\n")
	getSlice() // [4 6 10 14 22]

	fmt.Printf("Demonstrating: Working with 2D slices\n")
	slices2D() // [[2 4] [6 8]]

	fmt.Printf("Demonstrating: Compiling and running Lua code\n")
	compiledLua() // [[2 -2] [-2 2]] [2 4 6] Hello From Lua !

	fmt.Printf("Demonstrating: Handling Lua errors from Go code\n")
	handleErrors() // See, no error !

	fmt.Printf("Demonstrating: Restricting lua modules to prevent side-effects\n")
	restrictions() // Thanks Norton Antivirus!
}

func runFile() {
	L := lua.NewState()
	defer L.Close()

	err := L.DoFile("greeter.lua")
	if err != nil {
		return
	}
}

func callLua() {
	var name = "Howard"
	L := lua.NewState()
	defer L.Close()

	if err := L.DoFile("greeter2.lua"); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("say_hello"),
		NRet:    0,
		Protect: true,
	}, lua.LString(name)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}
}

func sayHello(L *lua.LState) int {
	name := L.ToString(1)
	fmt.Printf("Hello from Go! %s!\n", name)
	return 0
}

func callGo() {
	L := lua.NewState()
	defer L.Close()

	L.SetGlobal("say_hello", L.NewFunction(sayHello))
	if err := L.DoFile("greeter3.lua"); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}
}

func passSlice() {
	L := lua.NewState()
	defer L.Close()

	if err := L.DoFile("list_ops.lua"); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var slice = []int{2, 3, 5, 7, 11}

	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("print_list"),
		NRet:    0,
		Protect: true,
	}, luar.New(L, slice)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}
}

func asInt(lv lua.LValue) (int, bool) {
	var vLN, ok = lv.(lua.LNumber)
	if !ok {
		return 0, false
	}
	return int(math.Round(float64(vLN))), true
}

// Would you like a slice of curry ?
func asSlice[T any](cast func(value lua.LValue) (T, bool)) func(lua.LValue) ([]T, bool) {
	return func(lv lua.LValue) ([]T, bool) {
		if lv.Type() != lua.LTTable {
			println(fmt.Errorf("LValue is not a LTable"))
			return nil, false
		}
		var table = lv.(*lua.LTable)
		var length = table.Len()
		var slice = make([]T, length)
		var valid = true
		var cause error = nil
		table.ForEach(func(iRaw, vRaw lua.LValue) {
			var iLN, ok1 = iRaw.(lua.LNumber)
			if !ok1 {
				valid = false
				cause = fmt.Errorf("index is not a number")
				return
			}
			var i = int(math.Round(float64(iLN))) - 1 // indices start at one, baby :)
			var v, ok2 = cast(vRaw)
			if !ok2 {
				valid = false
				cause = fmt.Errorf("cast not successful")
				return
			}
			if i >= length || i < 0 {
				valid = false
				cause = fmt.Errorf("index is out of bounds")
				return
			}
			slice[i] = v
		})
		if !valid {
			println(cause)
			return nil, false
		}
		return slice, true
	}
}

func getSlice() {
	L := lua.NewState()
	defer L.Close()

	if err := L.DoFile("list_ops.lua"); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var slice = []int{2, 3, 5, 7, 11}

	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("double_list"),
		NRet:    1,
		Protect: true,
	}, luar.New(L, slice)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var doubled, ok = asSlice(asInt)(L.Get(-1))
	if !ok {
		fmt.Printf("conversion error\n")
		return
	}

	fmt.Printf("%v\n", doubled)
}

func slices2D() {
	L := lua.NewState()
	defer L.Close()

	if err := L.DoFile("list_ops.lua"); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var slice2d = [][]int{{1, 2}, {3, 4}}
	if err := L.CallByParam(lua.P{
		Fn:      L.GetGlobal("double_2D_list"),
		NRet:    1,
		Protect: true,
	}, luar.New(L, slice2d)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var doubled, ok = asSlice(asSlice(asInt))(L.Get(-1))
	if !ok {
		fmt.Printf("conversion error\n")
		return
	}

	fmt.Printf("%v\n", doubled)
}

func CompileLuaFromFile(filePath string) (*lua.FunctionProto, error) {
	file, err := os.Open(filePath)
	defer file.Close()
	if err != nil {
		return nil, err
	}
	reader := bufio.NewReader(file)
	return compileLua(reader, filePath)
}

func CompileLuaFromString(code string, name string) (*lua.FunctionProto, error) {
	var reader = strings.NewReader(code)
	return compileLua(reader, name)
}

// CompileLua reads the passed lua file from disk and compiles it.
func compileLua(reader io.Reader, name string) (*lua.FunctionProto, error) {
	chunk, err := parse.Parse(reader, name)
	if err != nil {
		return nil, err
	}
	proto, err := lua.Compile(chunk, name)
	if err != nil {
		return nil, err
	}
	return proto, nil
}

// DoCompiledLua takes a FunctionProto, as returned by CompileLua, and runs it in the LState. It is equivalent
// to calling DoFile on the LState with the original source file.
func DoCompiledLua(L *lua.LState, proto *lua.FunctionProto) error {
	lfunc := L.NewFunctionFromProto(proto)
	L.Push(lfunc)
	return L.PCall(0, lua.MultRet, nil)
}

func compiledLua() {
	var compiledFile, err1 = CompileLuaFromFile("list_ops.lua")
	if err1 != nil {
		fmt.Printf("error %s\n", err1)
		return
	}

	// Try a first instance
	var L1 = lua.NewState()
	defer L1.Close()
	if err := DoCompiledLua(L1, compiledFile); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var slice2d = [][]int{{1, -1}, {-1, 1}}
	if err := L1.CallByParam(lua.P{
		Fn:      L1.GetGlobal("double_2D_list"),
		NRet:    1,
		Protect: true,
	}, luar.New(L1, slice2d)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var doubled1, ok1 = asSlice(asSlice(asInt))(L1.Get(-1))
	if !ok1 {
		fmt.Printf("conversion error\n")
		return
	}

	fmt.Printf("%v ", doubled1)

	// Try a second instance
	var L2 = lua.NewState()
	defer L2.Close()
	if err := DoCompiledLua(L2, compiledFile); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var slice = []int{1, 2, 3}
	if err := L1.CallByParam(lua.P{
		Fn:      L1.GetGlobal("double_list"),
		NRet:    1,
		Protect: true,
	}, luar.New(L1, slice)); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

	var doubled2, ok2 = asSlice(asInt)(L1.Get(-1))
	if !ok2 {
		fmt.Printf("conversion error\n")
		return
	}

	fmt.Printf("%v ", doubled2)

	// Try directly from a string
	var compiledString, err2 = CompileLuaFromString(`print("Hello From Lua !")`, "greetings")
	if err2 != nil {
		fmt.Printf("error %s\n", err2)
		return
	}

	var L3 = lua.NewState()
	defer L3.Close()
	if err := DoCompiledLua(L3, compiledString); err != nil {
		fmt.Printf("error %s\n", err)
		return
	}

}

func handleErrors() {
	L := lua.NewState()
	defer L.Close()

	if err := L.DoFile("oopsie.lua"); err != nil {
		fmt.Printf("See, no errors!\n")
		return
	} else {
		fmt.Printf("I failed so hard, there is no errors...\n")
	}
}

type modules []struct {
	n string
	f lua.LGFunction
}

func loadModules(L *lua.LState, modules modules) {
	for _, pair := range modules {
		if err := L.CallByParam(lua.P{
			Fn:      L.NewFunction(pair.f),
			NRet:    0,
			Protect: true,
		}, lua.LString(pair.n)); err != nil {
			panic(err)
		}
	}
}

func restrictions() {
	L := lua.NewState(lua.Options{SkipOpenLibs: true})
	defer L.Close()

	loadModules(L, modules{
		{lua.LoadLibName, lua.OpenPackage},
		{lua.BaseLibName, lua.OpenBase},
		{lua.TabLibName, lua.OpenTable},
		{lua.MathLibName, lua.OpenMath},
		{lua.StringLibName, lua.OpenString},
	})

	if err := L.DoFile("virus.lua"); err != nil {
		fmt.Printf("Thanks Norton Antivirus !\n")
		return
	}
}
