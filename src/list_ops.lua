local write = io.write

print_list = function(list)
    write("[")
    for i, v in list() do
        write(v)
        if i ~= #list then write(" ") end
    end
    write(']\n')
end

double_list = function(list)
    local doubled_list = {}
    for i, v in list() do
        doubled_list[i] = 2*v
    end
    return doubled_list
end

double_2D_list = function(list_of_lists)
    local doubled_list_of_lists = {}
    for i, list in list_of_lists() do
        doubled_list_of_lists[i] = {}
        for j, v in list() do
            doubled_list_of_lists[i][j] = 2*v
        end
    end
    return doubled_list_of_lists
end