This is a small project to demonstrate a few of the capabilities of GopherLua
- Running Lua from a file
- Calling Lua code from Go
- Calling Go code from Lua
- Passing a map/slice as argument from Go to Lua
- Returning a map/slice from Lua to Go
- Working with 2D slices
- Compiling and Running Lua code # Compiled Lua file is not a binary, it is not useful for sharing across nodes
- Handling Lua errors from Go code
- Restricting lua modules to prevent side-effects
